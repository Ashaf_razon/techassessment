package com.nezamulislamar.tvmazetechnical.ui;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.EditText;

import com.nezamulislamar.tvmazetechnical.R;
import com.nezamulislamar.tvmazetechnical.data.ListOFSearchProducts;
import com.nezamulislamar.tvmazetechnical.data.SearchDataListSetInfo;
import com.nezamulislamar.tvmazetechnical.data.SearchParamGetterSetter;
import com.nezamulislamar.tvmazetechnical.network_call.SearchViewModel;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserDashboard extends AppCompatActivity {

    int getUserType = 1;
    int getUserTypeSearch = 2;
    private static final String PARENT_URL = "https://api.tvmaze.com/schedule/full";
    private static final String PARENT_SEARCH_URL = "https://api.tvmaze.com/singlesearch/shows?q=";//+ Show name
    private static final String SEARCH_ALL_PRODUCT = "ALL";
    private static final String LOG_MSG = "USER_DASHBOARD";
    private static final String INTETN_KEY_U_NAME = "EXTRA_USERNAME";
    private static final String INTETN_KEY_U_U_NAME = "EXTRA_USER_U_NAME";
    private static final String INTETN_KEY_U_PHONE = "EXTRA_USER_U_PHONE";
    private static final int INTERNET_DIALOG_FLAG = 1;
    private static final int LOG_OUT_DIALOG = 1;
    private Context context;
    private String username_string = "";
    private String user_username_string = "";
    private String user_phone_string = "";
    private LinearLayoutManager mLayoutManager;
    private int state;

    @BindView(R.id.recycler_view_main_product_list)
    RecyclerView recyclerView;
    @BindView(R.id.searchItems)
    EditText searchEdit;
    //Network
    private SearchParamGetterSetter searchParamGetterSetter;
    private SearchViewModel searchViewModel;
    private Observer<SearchDataListSetInfo> trackSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        context = UserDashboard.this;
        ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();
        username_string = extras.getString(INTETN_KEY_U_NAME);
        user_username_string = extras.getString(INTETN_KEY_U_U_NAME);
        user_phone_string = extras.getString(INTETN_KEY_U_PHONE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Network Initiate
        try{
            searchViewModel = ViewModelProviders.of(UserDashboard.this).get(SearchViewModel.class);
            trackSearch = searchDataListSetInfo -> initRecyclerView(searchDataListSetInfo);
        }catch (Exception e){
            Log.d(LOG_MSG+"net_con_e",e.getMessage());
        }
        callForListProduct(getUserType,SEARCH_ALL_PRODUCT, SEARCH_ALL_PRODUCT,PARENT_URL);
    }

    @Override
    public void onBackPressed() {
        logoutDialog(R.string.st_warn_exit,LOG_OUT_DIALOG);

    }

    public void viewUserProfile(View view) {
        try {
            Intent intentGoUserProf = new Intent(context, UserProfile.class);
            Bundle extras = new Bundle();
            extras.putString(INTETN_KEY_U_NAME, username_string);
            extras.putString(INTETN_KEY_U_U_NAME, user_username_string);
            extras.putString(INTETN_KEY_U_PHONE, user_phone_string);
            intentGoUserProf.putExtras(extras);
            startActivity(intentGoUserProf);
        } catch (Exception e) {
            Log.d(LOG_MSG, e.toString());
        }
    }

    private void logoutDialog(int dialogTitle, int dialogFlag) {
        try{
            mDialogClass myDialog = new mDialogClass((Activity) context,dialogTitle,dialogFlag, context);
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            myDialog.setCancelable(true);
            myDialog.show();
        }catch (Exception e){
            Log.d(LOG_MSG+" dialog",e.getMessage());
        }
    }

    private void initRecyclerView(SearchDataListSetInfo searchDataListSetInfo) {
        try{
            ListOFSearchProducts adapter = new ListOFSearchProducts(searchDataListSetInfo,context,getUserType);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            try{
                LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
                recyclerView.setLayoutAnimation(animation);
                Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
                recyclerView.scheduleLayoutAnimation();
            }catch (NullPointerException e){
                Log.d("SearchProducts",""+e.getMessage());
            }
        }catch (Exception e){
            Log.d(LOG_MSG+" rInit Er",e.getMessage());
        }
    }

    public boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    private void internetDialog() {
        mInternetCheckDialog myDialog = new mInternetCheckDialog((Activity) context,R.string.st_internet_warn,INTERNET_DIALOG_FLAG);
        try{
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }catch (Exception e){
            Log.d(LOG_MSG+" internet",e.getMessage());
        }
        myDialog.setCancelable(false);
        myDialog.show();
    }

    private void searchProductwithSuggestion(String spDepart, String spSearchBy, String s) {
        searchParamGetterSetter = new SearchParamGetterSetter();
        searchParamGetterSetter.setCompName(spSearchBy);
        searchParamGetterSetter.setDeptName(spDepart);
        searchParamGetterSetter.setStatusName(s);
        try{
            searchViewModel.getSearchListDataConfig(getUserTypeSearch, searchParamGetterSetter).observe(UserDashboard.this,trackSearch);
        }catch (Exception e){
            Log.d(LOG_MSG+" SRC_P_ER",e.getMessage());
        }
    }

    private void callForListProduct(int i, String spDepart, String spSearchBy, String searchValue) {
        searchProductwithSuggestion(SEARCH_ALL_PRODUCT,spSearchBy,searchValue);
    }

    public void searchMainpageProduct(View view) {
        if(isNetworkAvailable()){
            String getSearchValue = searchEdit.getText().toString();
            if(getSearchValue.length() == 0){
                getSearchValue = SEARCH_ALL_PRODUCT;
            }
            //callForListProduct(0,null, "2",PARENT_SEARCH_URL+getSearchValue);
        }else {
            internetDialog();
        }
    }
}