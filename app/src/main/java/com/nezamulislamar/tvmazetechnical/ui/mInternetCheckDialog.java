package com.nezamulislamar.tvmazetechnical.ui;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.nezamulislamar.tvmazetechnical.R;

public class mInternetCheckDialog extends Dialog implements View.OnClickListener {

    public Activity activity;
    public Button yes;
    private TextView txtDia;
    private int titleDial, flagNo;
    private static final String INTERNET_MSG = "INTERNET_DIAL_TITLE";

    public mInternetCheckDialog(Activity activity,int titleDial, int flagNo) {
        super(activity);
        this.activity = activity;
        this.titleDial = titleDial;
        this.flagNo = flagNo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.internet_warn_dialog);

        txtDia = findViewById(R.id.textViewInter);
        try{
            txtDia.setText(titleDial);
        }catch (Exception e){
            Log.d(INTERNET_MSG,""+e.getMessage());
        }
        yes = (Button) findViewById(R.id.internOkay);
        yes.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }
}
