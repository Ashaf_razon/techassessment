package com.nezamulislamar.tvmazetechnical.ui;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;
import com.nezamulislamar.tvmazetechnical.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIMING = 2000;
    @BindView(R.id.SplashProgressBar)
    public ProgressBar splashProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, LoginPage.class));
                finish();
            }
        }, SPLASH_TIMING);
    }

    @Override
    public void finish() {
        super.finish();
        splashProgress.setVisibility(View.GONE);
    }
}