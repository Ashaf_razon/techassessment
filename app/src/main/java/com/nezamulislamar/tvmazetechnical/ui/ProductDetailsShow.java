package com.nezamulislamar.tvmazetechnical.ui;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nezamulislamar.tvmazetechnical.R;
import com.nezamulislamar.tvmazetechnical.data.ProductDetailsGetterSetter;
import com.nezamulislamar.tvmazetechnical.data.SearchDataListSetInfo;
import com.nezamulislamar.tvmazetechnical.network_call.ProductDetailsViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductDetailsShow extends AppCompatActivity {
    private static final String LOG_MSG = "PRODUCT_DETAILS_PAGE";

    private String USER_TYPE_KEY = "USER";
    private final String ITEM_DESCRIPTION_KEY = "DESCRIPTION";
    private String productCode = "";
    private SearchDataListSetInfo searchDataListSetInfoSend;

    private Context context;
    @BindView(R.id.product_image)
    ImageView proImage;
    @BindView(R.id.product_title)
    TextView proTitle;
    @BindView(R.id.descCodeRes)
    TextView proCode;
    @BindView(R.id.descBrandRes)
    TextView proBrand;
    @BindView(R.id.descLocationRes)
    TextView proLoc;
    @BindView(R.id.descDeptRes)
    TextView proDept;
    @BindView(R.id.descNameRes)
    TextView proType;
    @BindView(R.id.descQtyRes)
    TextView proQty;
    @BindView(R.id.descDescriptionRes)
    TextView proDesc;
    @BindView(R.id.descYearRes)
    TextView proYear;
    @BindView(R.id.descUserRes)
    TextView proUser;
    @BindView(R.id.descPurchaseDateRes)
    TextView proPurchaseDate;
    @BindView(R.id.descProOutDateRes)
    TextView proOutDate;


    @BindView(R.id.descCode)
    TextView proCodeHead;
    @BindView(R.id.descBrand)
    TextView proBrandHead;
    @BindView(R.id.descLocation)
    TextView proLocHead;
    @BindView(R.id.descDept)
    TextView proDeptHead;
    @BindView(R.id.descName)
    TextView proTypeHead;
    @BindView(R.id.descQty)
    TextView proQtyHead;
    @BindView(R.id.descDescription)
    TextView proDescHead;
    @BindView(R.id.descYear)
    TextView proYearHead;
    @BindView(R.id.descUser)
    TextView proUserHead;
    @BindView(R.id.descPurchaseDate)
    TextView proPurchaseDateHead;
    @BindView(R.id.descProOutDate)
    TextView proOutDateHead;

    private int USER_TYPE = 0;

    //Network call
    private ProductDetailsViewModel productDetailsViewModel;
    private Observer<ProductDetailsGetterSetter> trackItemDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.descriptive_item_view);
        ButterKnife.bind(this);
        context = ProductDetailsShow.this;
        //Getting intent Result
        try{
            USER_TYPE = getIntent().getIntExtra(USER_TYPE_KEY,10);// show details
            productCode = getIntent().getStringExtra(ITEM_DESCRIPTION_KEY); // show code id
        }catch (Exception e){
            msgShow("ITEM DESCRIPTION INTENT ERROR: "+e.getMessage());
        }
        //Textview few are set GONE
        proBrand.setVisibility(View.GONE);
        proQty.setVisibility(View.GONE);
        proDesc.setVisibility(View.GONE);
        proYear.setVisibility(View.GONE);
        proPurchaseDate.setVisibility(View.GONE);

        proBrandHead.setVisibility(View.GONE);
        proQtyHead.setVisibility(View.GONE);
        proDescHead.setVisibility(View.GONE);
        proYearHead.setVisibility(View.GONE);
        proPurchaseDateHead.setVisibility(View.GONE);

        proCodeHead.setText("Show id ");
        proLocHead.setText("Run time ");
        proDeptHead.setText("Rating ");
        proUserHead.setText("Type ");
        proOutDateHead.setText("Summary ");


    }

    @Override
    protected void onStart() {
        super.onStart();
        productDetailsViewModel = ViewModelProviders.of(ProductDetailsShow.this).get(ProductDetailsViewModel.class);
        trackItemDetails = new Observer<ProductDetailsGetterSetter>(){
            @Override
            public void onChanged(@Nullable ProductDetailsGetterSetter productDetailsGetterSetter) {
                try{
                    Glide.with(context)
                            .load(productDetailsGetterSetter.getProImage())
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(proImage);
                    proTitle.setText(productDetailsGetterSetter.getProTitle());
                    proCode.setText(productDetailsGetterSetter.getProCode());
                    proBrand.setText(productDetailsGetterSetter.getProBrand());
                    proLoc.setText(productDetailsGetterSetter.getProLoc());
                    proDept.setText(productDetailsGetterSetter.getProDpt());
                    //proType.setText(productDetailsGetterSetter.getProType());
                    //proQty.setText(productDetailsGetterSetter.getProQty());
                    //proDesc.setText(productDetailsGetterSetter.getProDesc());
                    proYear.setText(productDetailsGetterSetter.getProYear());
                    //proUser.setText(productDetailsGetterSetter.getProUser());
                    //proPurchaseDate.setText(productDetailsGetterSetter.getProPurchaseDate());
                    proOutDate.setText(productDetailsGetterSetter.getProOutDate());
                }catch (Exception e){
                    msgShow("ITEM DESCRIPTION ERROR: "+e.getMessage());
                }
            }
        };
        productDetailsViewModel.getProductDetailsConfig(productCode,USER_TYPE).observe(ProductDetailsShow.this,trackItemDetails);

    }

    public void backProductDetailsClick(View view) {
        onBackPressed();
    }
    private void msgShow(String msg) {
        Toast.makeText(context,msg,Toast.LENGTH_LONG).show();
    }
}
