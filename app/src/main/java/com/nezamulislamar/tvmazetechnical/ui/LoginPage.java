package com.nezamulislamar.tvmazetechnical.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nezamulislamar.tvmazetechnical.R;
import com.nezamulislamar.tvmazetechnical.data.UserSigninImplementation;
import com.nezamulislamar.tvmazetechnical.presenter.SigninPresenter;
import com.nezamulislamar.tvmazetechnical.presenter.SigninView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginPage extends AppCompatActivity implements SigninView {

    private static String uId = null;
    private static String uPass = null;
    private static final String LOG_MSG = "LOGIN_PAGE";
    private static final String INTETN_KEY_U_NAME = "EXTRA_USERNAME";
    private static final String INTETN_KEY_U_U_NAME = "EXTRA_USER_U_NAME";
    private static final String INTETN_KEY_U_PHONE = "EXTRA_USER_U_PHONE";
    private static final int PD_TIMING = 3000;
    private Context context;

    @BindView(R.id.editAdminId)
    TextView getEditAdminId;
    @BindView(R.id.editAdminPass)
    TextView getEditAdminPass;
    @BindView(R.id.progressBarLogin)
    ProgressBar pd;

    private SigninPresenter signinPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        context = LoginPage.this;
        signinPresenter = new UserSigninImplementation(LoginPage.this, context);
        pd.setVisibility(View.GONE);
    }

    @OnClick(R.id.adminLogin)
    public void onLoginClick(View view) {
        pd.setVisibility(View.VISIBLE);
        new Thread(() -> {//background thread
            try {
                Thread.sleep(PD_TIMING);
                //take user input
                uId = getEditAdminId.getText().toString();
                uPass = getEditAdminPass.getText().toString();
                Log.d(LOG_MSG+" input",uId+" <> "+uPass);
                runOnUiThread(new Runnable(){//ui thread
                    public void run() {
                        signinPresenter.signIn(uId,uPass);
                        //signinPresenter.signIn("raz9","pass");
                        pd.setVisibility(View.GONE);
                    }
                });
            } catch (Exception e) {
                Log.d(LOG_MSG+" thread",e.getMessage());
                pd.setVisibility(View.GONE);
            }
        }).start();
    }

    @OnClick(R.id.createNewAcc)
    public void createNewClick(View view) {
        try{
            startActivity(new Intent(context, RegistrationPage.class));
        }catch (Exception e){
            Log.d(LOG_MSG+" create new",e.toString());
        }
    }

    @Override
    public void showValidationError() {
        Toast.makeText(context, "Flag! Fields must not be empty.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void signInSuccess(String uName, String userUserName, String uPhone) {
        Toast.makeText(context, "Congratulations! Sign in success.", Toast.LENGTH_LONG).show();
        try{
            Intent intentGoUserDash = new Intent(context, UserDashboard.class);
            Bundle extras = new Bundle();
            extras.putString(INTETN_KEY_U_NAME,uName);
            extras.putString(INTETN_KEY_U_U_NAME,userUserName);
            extras.putString(INTETN_KEY_U_PHONE,uPhone);
            intentGoUserDash.putExtras(extras);
            startActivity(intentGoUserDash);
        }catch (Exception e){
            Log.d(LOG_MSG+" login",e.toString());
        }
    }

    @Override
    public void signInError() {
        Toast.makeText(context, "Sorry! Sign in failed, Try again.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void dbConnError() {
        Toast.makeText(context, "Sorry! DB connection error.", Toast.LENGTH_LONG).show();
    }
}