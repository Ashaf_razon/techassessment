package com.nezamulislamar.tvmazetechnical.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.nezamulislamar.tvmazetechnical.R;
import com.nezamulislamar.tvmazetechnical.data.UserSigninImplementation;
import com.nezamulislamar.tvmazetechnical.presenter.SigninPresenter;
import com.nezamulislamar.tvmazetechnical.presenter.SigninView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserProfile extends AppCompatActivity{

    private static final String LOG_MSG = "USER_PROFILE";
    private static final String INTETN_KEY_U_NAME = "EXTRA_USERNAME";
    private static final String INTETN_KEY_U_U_NAME = "EXTRA_USER_U_NAME";
    private static final String INTETN_KEY_U_PHONE = "EXTRA_USER_U_PHONE";
    private String username_string = "";
    private String user_username_string = "";
    private String user_phone_string = "";

    @BindView(R.id.userProfileName)
    TextView userProfileNameTv;
    @BindView(R.id.userProfileUserName)
    TextView userProfileUserNameTv;
    @BindView(R.id.userProfilePhoneNo)
    TextView userProfilePhoneTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();
        username_string = extras.getString(INTETN_KEY_U_NAME);
        user_username_string = extras.getString(INTETN_KEY_U_U_NAME);
        user_phone_string = extras.getString(INTETN_KEY_U_PHONE);
        userProfileNameTv.setText("Name: "+username_string);
        userProfileUserNameTv.setText("User name: "+user_username_string);
        userProfilePhoneTv.setText("Phone: "+user_phone_string);
    }

    public void userProfileBackClick(View view) {
        try{
            finish();
        }catch (Exception e){
            Log.d(LOG_MSG,e.toString());
        }
    }
}