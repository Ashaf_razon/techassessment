package com.nezamulislamar.tvmazetechnical.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nezamulislamar.tvmazetechnical.R;
import com.nezamulislamar.tvmazetechnical.data.UserSigninImplementation;
import com.nezamulislamar.tvmazetechnical.presenter.SigninPresenter;
import com.nezamulislamar.tvmazetechnical.presenter.SigninView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegistrationPage extends AppCompatActivity implements SigninView {

    private String uName = null;
    private String uId = null;
    private String uPhone = null;
    private String uPass = null;
    private static final String LOG_MSG = "REGISTRATION";
    private static int PD_TIMING = 3000;
    private Context context;


    @BindView(R.id.editAdminName)
    TextView regEditAdminName;
    @BindView(R.id.editAdminUserName)
    TextView regEditAdminId;
    @BindView(R.id.editAdminPhone)
    TextView regEditAdminPhone;
    @BindView(R.id.editAdminPass)
    TextView regEditAdminPass;
    @BindView(R.id.progressBarReg)
    ProgressBar pd;

    private SigninPresenter signinPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        pd.setVisibility(View.GONE);
        context = RegistrationPage.this;
        signinPresenter = new UserSigninImplementation(RegistrationPage.this, context);
    }

    public void createNewUserClick(View view) {
        pd.setVisibility(View.VISIBLE);
        new Thread(() -> {//background thread
            try {
                Thread.sleep(PD_TIMING);
                //take user input
                uName = regEditAdminName.getText().toString();
                uId = regEditAdminId.getText().toString();
                uPass = regEditAdminPass.getText().toString();
                uPhone = regEditAdminPhone.getText().toString();
                Log.d(LOG_MSG,uName+" <> "+uId+" <> "+uPass+" <> "+uPhone);
                runOnUiThread(new Runnable(){ // ui thread
                    public void run() {
                        signinPresenter.userRegistration(uName, uId, uPhone,uPass);
                        pd.setVisibility(View.GONE);
                    }
                });
            } catch (Exception e) {
                Log.d(LOG_MSG+" thread",e.toString());
            }
        }).start();
    }

    @Override
    public void showValidationError() {
        Toast.makeText(context, "Flag! Fields must not be empty.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void signInSuccess(String uName, String userUserName, String uPhone) {
        Toast.makeText(context, "Congratulations! Registration success.", Toast.LENGTH_LONG).show();
        try{
            startActivity(new Intent(context, LoginPage.class));
            finish();
        }catch (Exception e){
            Log.d(LOG_MSG+" reg",e.toString());
            pd.setVisibility(View.GONE);
        }
    }

    @Override
    public void signInError() {
        Toast.makeText(context, "Sorry! Registration failed, Try again.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void dbConnError() {
        Toast.makeText(context, "Sorry! DB connection error.", Toast.LENGTH_LONG).show();
    }
}