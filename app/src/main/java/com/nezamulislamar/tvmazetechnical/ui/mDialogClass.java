package com.nezamulislamar.tvmazetechnical.ui;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.nezamulislamar.tvmazetechnical.R;

public class mDialogClass extends Dialog implements View.OnClickListener{

    private final Context context;
    private final Activity activity;
    private final int dialog_title;
    private final int flagNo;
    private Button yes, no;
    private TextView txtDia;
    private ProgressDialog pd;

    public mDialogClass(Activity activity, int dialog_title, int flagNo, Context context) {
        super(activity);
        //MasterActivity FLAG = 1 [Logout dialog/ Exit]
        //MasterActivity FLAG = 2 [Internet check]
        this.activity = activity;
        this.dialog_title = dialog_title;
        this.flagNo = flagNo;
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog);

        pd = new ProgressDialog(context);
        pd.setCancelable(false);
        pd.setIndeterminate(true);
        pd.setMessage("Loading...");
        pd.setTitle("Please wait.");

        yes = findViewById(R.id.button2);
        no =  findViewById(R.id.button);
        txtDia = findViewById(R.id.textView7);
        txtDia.setText(dialog_title);

        yes.setOnClickListener(this);
        no.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button2: //yes
                dismiss();
                activity.finish();
                break;
            case R.id.button: //No
                dismiss();
                break;
            default:
                break;
        }
    }
}

