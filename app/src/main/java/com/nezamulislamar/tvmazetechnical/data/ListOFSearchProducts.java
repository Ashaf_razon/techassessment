package com.nezamulislamar.tvmazetechnical.data;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nezamulislamar.tvmazetechnical.R;
import com.nezamulislamar.tvmazetechnical.ui.ProductDetailsShow;
import com.nezamulislamar.tvmazetechnical.ui.mInternetCheckDialog;
import com.squareup.picasso.Picasso;

public class ListOFSearchProducts extends RecyclerView.Adapter<ListOFSearchProducts.ViewHolder>{
    //var
    private static final int INTERNET_DIALOG_FLAG = 1;
    private int USER_TYPE = 0;
    private String USER_TYPE_KEY = "USER";
    private final String TAG = "ListOfsearchProd";
    private final String ITEM_DESCRIPTION_KEY = "DESCRIPTION";
    SearchDataListSetInfo searchDataListSetInfoSend;
    private Context context;

    public ListOFSearchProducts(@Nullable SearchDataListSetInfo SearchDataListSetInfo, Context context, int USER_TYPE) {
        this.searchDataListSetInfoSend = SearchDataListSetInfo;
        this.context = context;
        this.USER_TYPE = USER_TYPE;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_product,viewGroup,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") int i) {
        Log.d(TAG,"Bind View Holder Called");
        try{
            viewHolder.pCode.setText(searchDataListSetInfoSend.proCode.get(i));
            viewHolder.pName.setText(searchDataListSetInfoSend.proName.get(i));
            viewHolder.pDpt.setText(searchDataListSetInfoSend.proDpt.get(i));
            viewHolder.pUser.setText(searchDataListSetInfoSend.proUser.get(i));
            viewHolder.pYear.setText(searchDataListSetInfoSend.proYear.get(i));
            try{
                if(searchDataListSetInfoSend.proImage.get(i) != "null"){
                    Glide.with(this.context)
                            .load(searchDataListSetInfoSend.proImage.get(i))
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(viewHolder.pImage);
                }
            }catch (Exception e){
                Log.d("image error",e.getMessage());
            }
        }catch (Exception e){
            Log.d("bindView","error: "+e.getMessage());
        }
        viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    if(searchDataListSetInfoSend.getProName().get(i).compareToIgnoreCase("<No item>") == 0){
                        wrongDateDialog(R.string.noSearchResMsg);
                    }else {
                        if(isNetworkAvailable()){
                            Intent intent = new Intent(context, ProductDetailsShow.class);
                            intent.putExtra(ITEM_DESCRIPTION_KEY,searchDataListSetInfoSend.proCode.get(i));
                            intent.putExtra(USER_TYPE_KEY,USER_TYPE);
                            (context).startActivity(intent);
                        }else {
                            internetDialog();
                        }
                    }
                    Log.d("Descriptive_Intent",""+searchDataListSetInfoSend.proCode.size()+" <> "+USER_TYPE);
                }catch (Exception e){
                    Toast.makeText(context,"Descriptive Intent Error: "+e.getMessage(),Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return searchDataListSetInfoSend.getProName().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView pCode, pName, pDpt, pUser, pYear, lpYear, lpUser;
        ImageView pImage;
        RelativeLayout parentLayout;
        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            try{
                pCode = itemView.findViewById(R.id.lpCodeRes);
                pName = itemView.findViewById(R.id.lpNameRe);
                pDpt = itemView.findViewById(R.id.lpDptRes);
                pUser = itemView.findViewById(R.id.lpUserRes);
                pYear = itemView.findViewById(R.id.lpYearRes);
                pImage = itemView.findViewById(R.id.listProduct);
                lpYear = itemView.findViewById(R.id.lpYear);
                lpUser = itemView.findViewById(R.id.lpUser);

                pYear.setVisibility(View.GONE);
                pUser.setVisibility(View.GONE);
                lpUser.setVisibility(View.GONE);
                lpYear.setVisibility(View.GONE);
            }catch (Exception e){
                Toast.makeText(context,e.getMessage(),Toast.LENGTH_LONG).show();
                Log.d("TagListOFSearchProducts",""+e.getMessage());
            }
            parentLayout = itemView.findViewById(R.id.relative_Layout_Content_list);
        }
    }

    private void wrongDateDialog(int titleDialog) {
        mInternetCheckDialog myDialog = new mInternetCheckDialog((Activity) context, titleDialog,INTERNET_DIALOG_FLAG);
        try{
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        myDialog.setCancelable(false);
        myDialog.show();
    }

    public boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    private void internetDialog() {
        mInternetCheckDialog myDialog = new mInternetCheckDialog((Activity) context,R.string.st_internet_warn,INTERNET_DIALOG_FLAG);
        try{
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }catch (Exception e){
            e.printStackTrace();
        }
        myDialog.setCancelable(false);
        myDialog.show();
    }
}
