package com.nezamulislamar.tvmazetechnical.data;

import java.util.ArrayList;

public class SearchDataListSetInfo {
    public ArrayList<String> proName = new ArrayList<>();
    public ArrayList<String> proCode = new ArrayList<>();
    public ArrayList<String> proYear = new ArrayList<>();
    public ArrayList<String> proDpt = new ArrayList<>();
    public ArrayList<String> proUser = new ArrayList<>();
    public ArrayList<String> proImage = new ArrayList<>();

    public ArrayList<String> getProName() {
        return proName;
    }
    public ArrayList<String> getProCode () {
        return proCode ;
    }
    public ArrayList<String> getProYear() {
        return proYear;
    }
    public ArrayList<String> getProDpt() {
        return proDpt;
    }
    public ArrayList<String> getProUser() {
        return proUser;
    }
    public ArrayList<String> getProImage() {
        return proImage;
    }

    public void setProName(ArrayList<String> proName) {
        this.proName = proName;
    }

    public void setProCode (ArrayList<String> proCode) {
        this.proCode  = proCode ;
    }

    public void setProYear(ArrayList<String> proYear) {
        this.proYear = proYear;
    }

    public void setProDpt(ArrayList<String> proDpt) {
        this.proDpt = proDpt;
    }

    public void setProUser(ArrayList<String> proUser) {
        this.proUser = proUser;
    }

    public void setProImage(ArrayList<String> proImage) {
        this.proImage = proImage;
    }
}
