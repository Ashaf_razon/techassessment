package com.nezamulislamar.tvmazetechnical.data;

public class SearchParamGetterSetter {
    public String compName = null;
    public String deptName = null;
    public String statusName = null;
    public String dateForDate = null;
    public String dateFrom = null;
    public String dateTo = null;

    public void setStatusName(String statusName){
        this.statusName = statusName;
    }

    public void setCompName(String compName){
        this.compName = compName;
    }

    public void setDeptName(String deptName){
        this.deptName = deptName;
    }

    public void setDateForDate(String dateForDate){
        this.dateForDate = dateForDate;
    }

    public void setDateFrom(String dateFrom){
        this.dateFrom = dateFrom;
    }

    public void setDateTo(String dateTo){
        this.dateTo = dateTo;
    }

    public String getStatusName() {
        return statusName;
    }
    public String getCompName() {
        return compName;
    }
    public String getDeptName() {
        return deptName;
    }
    public String getDateForDate() {
        return dateForDate;
    }

    public String getDateFrom() {
        return dateFrom;
    }
    public String getDateTo() {
        return dateTo;
    }
}
