package com.nezamulislamar.tvmazetechnical.data;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;


public class LocalDatabaseHelper extends SQLiteOpenHelper {

    private static final String MSG_LOG = "MODEL_DB"; // logcat

    //DB creating
    private static final int DATABASE_VERSION = 1;// DB version
    private static final String DATABASE_NAME = "userRegistration"; //DB name
    private static final String TABLE_USER_REG = "registration";/// User registration Table
    //Table Column
    private static final String  U_ORIGINAL_NAME = "u_oname";
    private static final String U_USER_NAME = "u_uname";
    private static final String U_PHONE = "u_phone";
    private static final String U_PASS = "u_pass";
    //DB execute query
    //REG table create statement
    private static final String CREATE_TABLE_REG = "CREATE TABLE "
            + TABLE_USER_REG + "(" + U_PHONE + " INTEGER PRIMARY KEY," + U_ORIGINAL_NAME
            + " TEXT," + U_USER_NAME + " TEXT," + U_PASS
            + " TEXT" + ")";

    public LocalDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // creating required tables
        db.execSQL(CREATE_TABLE_REG);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_REG);
        onCreate(db);
    }

    //TODO ------create user Register input and fetch method------------
    //Insert data
    public int createRegistration(ModelRegistration mReg){
        int flag = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(U_PHONE, mReg.getuPhone());
        values.put(U_ORIGINAL_NAME, mReg.getuName());
        values.put(U_USER_NAME, mReg.getuUserName());
        values.put(U_PASS, mReg.getuPass());

        try{
            // insert row
            flag = 1;
            long todo_id = db.insert(TABLE_USER_REG, null, values);
            Log.d(MSG_LOG+" insert","DB insert-success, user created");
        }catch (Exception e){
            flag = 0;
            Log.d(MSG_LOG+" insert",e.getMessage());
        }
        return flag;
    }

    //Fetch data
    public ModelRegistration getRegisteredData(String uUserName, String uPass) {
        SQLiteDatabase db = this.getReadableDatabase();
        ModelRegistration mRegRead = new ModelRegistration();

        String selectQuery = "SELECT  * FROM " + TABLE_USER_REG + " WHERE "
                + U_USER_NAME + " = '" + uUserName+"' AND " + U_PASS + " = '" + uPass + "' LIMIT 1";
        Log.e(MSG_LOG, selectQuery);

        try{
            Cursor c = db.rawQuery(selectQuery, null);
            if (c != null){
                c.moveToFirst();
                mRegRead.setuName(c.getString(c.getColumnIndex(c.getColumnName(1))));
                mRegRead.setuUserName(c.getString(c.getColumnIndex(c.getColumnName(2))));
                mRegRead.setuPhone(c.getString(c.getColumnIndex(c.getColumnName(0))));
                mRegRead.setuPass(c.getString(c.getColumnIndex(c.getColumnName(3))));

                Log.d("r0",c.getString(c.getColumnIndex(c.getColumnName(1)))+"");
                Log.d("r1",c.getString(c.getColumnIndex(c.getColumnName(2)))+"");
                Log.d("r2",c.getString(c.getColumnIndex(c.getColumnName(0)))+"");
                Log.d("r3",c.getString(c.getColumnIndex(c.getColumnName(3)))+"");
            }else{
                mRegRead.setuName("");
                mRegRead.setuUserName("");
                mRegRead.setuPhone("");
                mRegRead.setuPass("");
                Log.d(MSG_LOG,c.toString());
            }
        }catch (Exception e){
            Log.d(MSG_LOG+" read",e.getMessage());
        }
        return mRegRead;
    }

    //Fetch all row [Testing]
    public ModelRegistration getRegisteredData() {
        SQLiteDatabase db = this.getReadableDatabase();
        ModelRegistration mRegRead = new ModelRegistration();

        String selectQuery = "SELECT  * FROM " + TABLE_USER_REG;
        Log.e(MSG_LOG, selectQuery);
        String tableString = String.format("Table %s:\n", TABLE_USER_REG);

        try{
            Cursor allRows = db.rawQuery(selectQuery, null);
            if (allRows.moveToFirst() ){
                String[] columnNames = allRows.getColumnNames();
                Log.d(MSG_LOG,columnNames.length+"");
                do {
                    for (String name: columnNames) {
                        tableString += String.format("%s: %s\n", name,
                                allRows.getString(allRows.getColumnIndex(name)));
                    }
                    tableString += "\n";

                } while (allRows.moveToNext());
            }

            Log.d(MSG_LOG+" read",tableString);
        }catch (Exception e){
            Log.d(MSG_LOG+" read",e.getMessage());
        }

        return mRegRead;
    }
    // closing database
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

}
