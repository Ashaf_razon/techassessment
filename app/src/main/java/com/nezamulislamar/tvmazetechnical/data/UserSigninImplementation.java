package com.nezamulislamar.tvmazetechnical.data;

import android.content.Context;
import android.util.Log;

import com.nezamulislamar.tvmazetechnical.presenter.SigninPresenter;
import com.nezamulislamar.tvmazetechnical.presenter.SigninView;

public class UserSigninImplementation implements SigninPresenter {
    private final SigninView signinView;
    //Database helper declaration
    private final LocalDatabaseHelper db;

    public UserSigninImplementation(SigninView signinView, Context context) {
        this.signinView = signinView;
        //DB init
        db = new LocalDatabaseHelper(context);
    }
    @Override
    public void signIn(String userName, String passWord) {
        if((userName == null || userName.length() == 0) || (passWord == null || passWord.length() == 0)){
            signinView.showValidationError();
        }else{
            try{
                //DB task
                ModelRegistration mReg = db.getRegisteredData(userName,passWord); //read user data
                db.closeDB();
                //DB task end
                Log.d("signin implement DB",mReg.getuUserName()+" <> "+mReg.getuPass());
                if(userName.compareTo(mReg.getuUserName()) == 0 && passWord.compareTo(mReg.getuPass()) == 0){
                    signinView.signInSuccess(mReg.getuName(), mReg.getuUserName(),mReg.getuPhone());
                }else{
                    signinView.signInError();
                }
            }catch (Exception e){
                Log.d("signin implement DB exp", e.getMessage());
                //signinView.dbConnError();
                signinView.signInError();
            }
        }
    }

    @Override
    public void userRegistration(String userName, String userUserName, String userPhone, String passWord) {
        if((userName == null || userName.length() == 0) || (userUserName == null || userUserName.length() == 0) || (userPhone == null || userPhone.length() == 0) || (passWord == null || passWord.length() == 0) ) {
            signinView.showValidationError();
        }else {
            try{
                //DB task
                ModelRegistration mReg = new ModelRegistration(userName, userUserName, userPhone, passWord);
                //inserting User into DB
                int usearCreatingFlag = db.createRegistration(mReg); //flag = 0 fail, 1 = success
                db.closeDB();
                //DB task end

                if(usearCreatingFlag == 1){
                    signinView.signInSuccess(mReg.getuName(), mReg.getuUserName(), mReg.getuPhone());
                }else {
                    signinView.signInError();
                }
            }catch (Exception e){
                Log.d("Reg implement DB exp", e.getMessage());
                signinView.dbConnError();
            }
        }
    }
}
