package com.nezamulislamar.tvmazetechnical.data;

public class ModelRegistration {
    String uName;
    String uUserName;
    String uPhone;
    String uPass;

    public ModelRegistration() {
    }
    public ModelRegistration(String uName, String uUserName, String uPhone, String uPass) {
        this.uName = uName;
        this.uUserName = uUserName;
        this.uPhone = uPhone;
        this.uPass = uPass;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getuUserName() {
        return uUserName;
    }

    public void setuUserName(String uUserName) {
        this.uUserName = uUserName;
    }

    public String getuPhone() {
        return uPhone;
    }

    public void setuPhone(String uPhone) {
        this.uPhone = uPhone;
    }

    public String getuPass() {
        return uPass;
    }

    public void setuPass(String uPass) {
        this.uPass = uPass;
    }
}
