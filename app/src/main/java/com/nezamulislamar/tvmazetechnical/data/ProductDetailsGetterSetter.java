package com.nezamulislamar.tvmazetechnical.data;

public class ProductDetailsGetterSetter {
    public String proImage = null;
    public String proTitle = null;
    public String proCode = null;
    public String proBrand = null;
    public String proLoc = null;
    public String proDpt = null;
    public String proType = null;
    public String proQty = null;
    public String proDesc = null;
    public String proYear = null;
    public String proUser = null;
    public String proPurchaseDate = null;
    public String proOutDate = null;

    public void setProImage(String proImage){
        this.proImage = proImage;
    }

    public String getProImage() {
        return proImage;
    }

    public void setProTitle(String proTitle){
        this.proTitle = proTitle;
    }

    public String getProTitle() {
        return proTitle;
    }

    public void setProCode(String proCode){
        this.proCode = proCode;
    }

    public String getProCode() {
        return proCode;
    }

    public void setProBrand(String proBrand){
        this.proBrand = proBrand;
    }

    public String getProBrand() {
        return proBrand;
    }

    public void setProLoc(String proLoc){
        this.proLoc = proLoc;
    }

    public String getProLoc() {
        return proLoc;
    }

    public void setProDpt(String proDpt){
        this.proDpt = proDpt;
    }

    public String getProDpt() {
        return proDpt;
    }

    public void setProType(String proType){
        this.proType = proType;
    }

    public String getProType() {
        return proType;
    }

    public void setProQty(String proQty){
        this.proQty = proQty;
    }

    public String getProQty() {
        return proQty;
    }

    public void setProDesc(String proDesc){
        this.proDesc = proDesc;
    }

    public String getProDesc() {
        return proDesc;
    }

    public void setProYear(String proYear){
        this.proYear = proYear;
    }

    public String getProYear() {
        return proYear;
    }

    public void setProUser(String proUser){
        this.proUser = proUser;
    }

    public String getProUser() {
        return proUser;
    }

    public void setProPurchaseDate(String proPurchaseDate){
        this.proPurchaseDate = proPurchaseDate;
    }

    public String getProPurchaseDate() {
        return proPurchaseDate;
    }

    public void setProOutDate(String proOutDate){
        this.proOutDate = proOutDate;
    }

    public String getProOutDate() {
        return proOutDate;
    }
}
