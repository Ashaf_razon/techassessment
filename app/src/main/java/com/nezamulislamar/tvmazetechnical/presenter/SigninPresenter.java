package com.nezamulislamar.tvmazetechnical.presenter;

public interface SigninPresenter {
    void signIn(String userName, String passWord);
    void userRegistration(String userName, String userUserName, String userPhone, String passWord);
}
