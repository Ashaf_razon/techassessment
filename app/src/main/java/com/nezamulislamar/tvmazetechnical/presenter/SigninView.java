package com.nezamulislamar.tvmazetechnical.presenter;

public interface SigninView {
    void showValidationError();
    void signInSuccess(String uName, String userUserName, String uPhone);
    void signInError();
    void dbConnError();
}
