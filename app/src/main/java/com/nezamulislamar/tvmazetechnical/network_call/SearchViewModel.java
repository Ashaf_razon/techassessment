package com.nezamulislamar.tvmazetechnical.network_call;

import android.app.Application;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nezamulislamar.tvmazetechnical.data.SearchDataListSetInfo;
import com.nezamulislamar.tvmazetechnical.data.SearchParamGetterSetter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SearchViewModel extends AndroidViewModel {

    //private static final String PARENT_URL = "https://api.tvmaze.com/schedule/full";
    //private static final String PARENT_SEARCH_URL = "https://api.tvmaze.com/singlesearch/shows?q=";//+ Show name
    private String urlInit = "";
    private int flag;


    private ArrayList<String> proName = new ArrayList<>();
    private ArrayList<String> proCode = new ArrayList<>();
    private ArrayList<String> proYear = new ArrayList<>();
    private ArrayList<String> proDpt = new ArrayList<>();
    private ArrayList<String> proUser = new ArrayList<>();
    public ArrayList<String> proImage = new ArrayList<>();

    private SearchParamGetterSetter srcParamGetterSetter;
    public SearchDataListSetInfo setSearchListData = new SearchDataListSetInfo();
    public MutableLiveData<SearchDataListSetInfo> searchListDataConfig;
    public MutableLiveData<SearchDataListSetInfo> getSearchListDataConfig(int FLAG_PARAM, SearchParamGetterSetter searchParamGetterSetter){
        this.srcParamGetterSetter = searchParamGetterSetter;
        this.flag = FLAG_PARAM;
        urlInit = searchParamGetterSetter.getStatusName();

        initSizeZero();
        if(searchListDataConfig == null){
            searchListDataConfig = new MutableLiveData<>();
            //Log.d("GET_ACTIVITY_SEARCH","Config. Call");
            getCommonDataInfo();
        }else{
            searchListDataConfig = new MutableLiveData<>();
            //Log.d("GET_ACTIVITY_SEARCH","Config. Call -> Not Null");
            getCommonDataInfo();
        }
        return searchListDataConfig;
    }

    private void getCommonDataInfo() {
        // creating a new variable for our request queue
        Log.d("response_search_net-01", "Search-Page: " + urlInit);
        RequestQueue queue = Volley.newRequestQueue(getApplication());
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, urlInit, null, response -> {
            Log.d("response_search_net-01", "Search-Page: " + response);
            for (int i = 0; i < response.length(); i++) {
                try {
                    // we are getting each json object.
                    JSONObject jsonObject = response.getJSONObject(i);
                    if(jsonObject.getString("image") != null || jsonObject.getString("name") != null
                            || jsonObject.getString("id") != null || jsonObject.getString("rating") != null){
                        if(jsonObject.getString("image") != "null"){
                            proImage.add(jsonObject.getJSONObject("image").getString("medium"));
                        }else{
                            proImage.add(jsonObject.getString("image"));
                        }
                        proDpt.add(jsonObject.getJSONObject("rating").getString("average"));
                        proName.add(jsonObject.getString("name"));
                        proCode.add(jsonObject.getString("id"));
                        proYear.add(jsonObject.getString("type"));
                        proUser.add(jsonObject.getString("summary"));

                        setSearchListData.setProName(proName);
                        setSearchListData.setProCode(proCode);
                        setSearchListData.setProDpt(proDpt);
                        setSearchListData.setProYear(proYear);
                        setSearchListData.setProUser(proUser);
                        setSearchListData.setProImage(proImage);
                        searchListDataConfig.setValue(setSearchListData);

                        //Log.d("response_search_net-02", "Success");
                    }
                } catch (JSONException e) {
                    Log.d("response_search_net", "Error Listener: " +e.getMessage());
                }
            }
        }, error -> Log.d("volly error", "Error Listener: " +error.getMessage()));
        queue.add(jsonArrayRequest);
    }
    public SearchViewModel(@NonNull Application application) {
        super(application);
    }

    private void initSizeZero(){
        proName.clear();
        proCode.clear();
        proDpt.clear();
        proYear.clear();
        proUser.clear();
        proImage.clear();
    }
}
