package com.nezamulislamar.tvmazetechnical.network_call;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nezamulislamar.tvmazetechnical.data.ProductDetailsGetterSetter;

import org.json.JSONArray;
import org.json.JSONObject;


public class ProductDetailsViewModel extends AndroidViewModel {
    private static String PARENT_ADD = "https://api.tvmaze.com/schedule/full";//"https://api.tvmaze.com/shows/1929";

    private String commonURL;
    private String proImage;
    private String proTitle;
    private String proCode;
    private String proBrand;
    private String proLoc;
    private String proDpt;
    private String proType;
    private String proQty;
    private String proDesc;
    private String proYear;
    private String proUser;
    private String proPurchaseDate;
    private String proOutDate;

    private String getProCode;
    private int userType;
    public ProductDetailsGetterSetter productDetailsGetterSetter = new ProductDetailsGetterSetter();
    public MutableLiveData<ProductDetailsGetterSetter> productDetailsConfig;
    public MutableLiveData<ProductDetailsGetterSetter> getProductDetailsConfig(String getProCode, int userType){
        this.getProCode = getProCode;
        this.userType = userType;

        //get product code and call network
        commonURL = PARENT_ADD;

        Log.d("GET_ACTIVITY","Config. "+commonURL);
        if(productDetailsConfig == null){
            productDetailsConfig = new MutableLiveData<>();
            //Log.d("GET_ACTIVITY_SEARCH","Config. Call");
            getDetailsDataInfo();
        }else{
            productDetailsConfig = new MutableLiveData<>();
            //Log.d("GET_ACTIVITY_SEARCH","Config. Call -> Not Null");
            getDetailsDataInfo();
        }

        return productDetailsConfig;
    }

    private void getDetailsDataInfo() {
        StringRequest request = new StringRequest(Request.Method.GET, commonURL, response -> {
            Log.d("response_search_net-1", "Search-Page: " + response);
            try {
                if(response.length() > 5){
                    JSONArray jsonArray = new JSONArray(response);
                    int count  = 0;
                    while(count < jsonArray.length()){
                        JSONObject jsonObject = jsonArray.getJSONObject(count);

                        if(jsonObject.getString("id").compareTo(getProCode) == 0){
                            if(jsonObject.getString("image") != "null"){
                                productDetailsGetterSetter.setProImage(jsonObject.getJSONObject("image").getString("medium"));
                            }else{
                                productDetailsGetterSetter.setProImage(jsonObject.getString("image"));
                            }
                            proTitle = jsonObject.getString("name");
                            proCode = jsonObject.getString("id");
                            //proBrand = jsonObject.getString("genres");
                            proLoc = jsonObject.getString("runtime");
                            proDpt = jsonObject.getJSONObject("rating").getString("average");
                            proUser = jsonObject.getString("type");
                            proOutDate = jsonObject.getString("summary");

                            productDetailsGetterSetter.setProTitle(proTitle);
                            productDetailsGetterSetter.setProCode(proCode);
                            //productDetailsGetterSetter.setProBrand(proBrand);
                            productDetailsGetterSetter.setProLoc(proLoc);
                            productDetailsGetterSetter.setProDpt(proDpt);
                            productDetailsGetterSetter.setProYear(proYear);
                            productDetailsGetterSetter.setProOutDate(proOutDate);

                            productDetailsConfig.setValue(productDetailsGetterSetter);
                            Log.d("response_search_net-01",getProCode+" "+proTitle+" "
                                    +proCode+" "+proBrand+" "+proLoc+" "+proDpt+" "+proUser+" "+proOutDate+" "+
                                    jsonObject.getString("image")+" "+
                                    jsonObject.getJSONObject("image").getString("medium"));
                            break;
                        }
                        Log.d("response_search_net-00",getProCode+" <> "+jsonObject.getString("id"));
                        count++;
                    }
                }else {
                    proImage = "https://static.tvmaze.com/uploads/images/medium_portrait/81/202632.jpg";
                    proTitle = "<sorry>";
                    proCode = "<sorry>";
                    proBrand = "<sorry>";
                    proLoc = "<sorry>";
                    proDpt = "<sorry>";
                    proType = "<sorry>";
                    proQty = "<sorry>";
                    proDesc = "<sorry>";
                    proYear = "<sorry>";
                    proUser = "<sorry>";
                    proPurchaseDate = "<sorry>";
                    proOutDate = "<sorry>";

                    productDetailsGetterSetter.setProTitle(proTitle);
                    productDetailsGetterSetter.setProImage(proImage);
                    productDetailsGetterSetter.setProCode(proCode);
                    productDetailsGetterSetter.setProBrand(proBrand);
                    productDetailsGetterSetter.setProLoc(proLoc);
                    productDetailsGetterSetter.setProDpt(proDpt);
                    productDetailsGetterSetter.setProType(proType);
                    productDetailsGetterSetter.setProQty(proQty);
                    productDetailsGetterSetter.setProDesc(proDesc);
                    productDetailsGetterSetter.setProUser(proUser);
                    productDetailsGetterSetter.setProYear(proYear);
                    productDetailsGetterSetter.setProPurchaseDate(proPurchaseDate);
                    productDetailsGetterSetter.setProOutDate(proOutDate);

                    productDetailsConfig.setValue(productDetailsGetterSetter);
                }

                Log.d("response_search_net-2", "Success");
            } catch (Exception e) {
                Log.d("response_search_net-3", "Exception: " + e.getMessage());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("response_search_net", "Error Listener: " +error.getMessage());
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplication());
        Log.d("response_search_net", "out-context: " + getApplication());
        requestQueue.add(request);
    }

    public ProductDetailsViewModel(@NonNull Application application) {
        super(application);
    }
}
